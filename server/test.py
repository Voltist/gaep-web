import urllib2
from datetime import datetime, date, timedelta

now = datetime.now()- timedelta(days=3)
past = now + timedelta(days=6)

data = urllib2.urlopen('https://earthquake.usgs.gov/fdsnws/event/1/query?format=text&starttime=' + str(past.date()) + '&endtime=' + str(now.date()) + '&minmagnitude=4').read()
print data
