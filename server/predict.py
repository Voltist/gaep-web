import shutil
import urllib2
from contextlib import closing
import sys
from math import sin, cos, sqrt, atan2, radians
import pickle as pkl
import time as tting
from datetime import datetime, timedelta, date
import os
import pymongo
from pymongo import MongoClient

# Connect to db
authstr = 'mongodb://%s:%s@localhost:27017/gaep' % ("admin", os.environ['gaep_db_pass'])
client = MongoClient(authstr)
print authstr
db = client.gaep
collection = db.predictions

# Months for unused latitude prediction
months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

# A utility for the time_to_lon function
def clamp(num):
    if num > 180:
        return -180 + (num-180)
    if num < -180:
        return 180 + (num+180)
    else:
        return num

# A utility that checks if a longitude is between two others assuming that the areas spans eastward
def check_lon_between(l0, l1, p):
    tmp = [l0]
    while tmp[-1] != l1:
        tmp.append(clamp(tmp[-1] + 1))

    if p in tmp:
        return True
    else:
        return False

# Unused latitude prediction
def month_to_lat(month):
    lat = -23*cos(radians(month*(360.0/12.0)))
    return lat
def month_to_lat_rev(month):
    return 0-month_to_lat(month)

# Get a pair of longitudes from a given hour of day
def time_to_lon(utc_input):
    tmp = 12 - utc_input
    start_long_unrev = tmp * 15
    start_long = start_long_unrev # In degrees east
    middle_long = start_long + 22 # Adjusted for time delay between orbit and earths surface (the coolest comment i have ever written)
    print middle_long
    return [int(clamp(middle_long-90)), int(clamp(middle_long+90))]

# This is where all the magic happens. All dates/datetimes are strings to make thing easier on the javascript side of things later.
def make_prediction(dt, kp):

    # Round hour up and predict longitude
    hour = int(dt.hour) + 1
    lons = time_to_lon(hour)

    # Get closest and furthest possible dates
    dates = [str((dt + timedelta(days=3)).date()), str((dt + timedelta(days=5)).date())]

    # Get magnitude range
    mags = [(float(kp)/2)+3.5, (float(kp)/2)+4.5]

    # Return predictions and prediction time
    return {"prediction_time": str(dt), "dates": dates, "mags": mags, "lons": lons}



# Main loop
while True:

    if len(sys.argv) == 1 or sys.argv[1] == "--offset":
        # Get current month's alerts from FTP server
        try:
            with closing(urllib2.urlopen('ftp://ftp.swpc.noaa.gov/pub/alerts/current_month.html')) as r:
                with open('tmp.html', 'wb') as f:
                    shutil.copyfileobj(r, f)

            with open("tmp.html", "r") as fp:
                data = fp.read()

        except:
            # Wait for error to pass before trying again
            tting.sleep(300)
            continue

    else:
        # Load data from test file
        with open("test.html", "r") as fp:
            data = fp.read()

    # Get indyvidual events
    raw_events = data.split("<hr>")[1:]
    print "."

    # Get offset if needed
    if len(sys.argv) == 2 and sys.argv[1] == "--offset":
        e_offset = int(raw_input("Offset?: "))
    else:
        e_offset = 0

    # Get most recent event
    info = raw_events[e_offset].strip("\n").split("<p>")[-1].split("<br>")

    # Check that alert is the kind that we are looking for
    if "ALERT: Geomagnetic K-index of" in info[0] and "expected" not in info[0]:
        # Get kp from alert and check that it meets the required threshold
        kp = int(info[0].replace("ALERT: Geomagnetic K-index of ", ""))
        if kp >= 5:

            # Get datetime and make prediction
            dt = datetime.strptime(info[1].split(": ")[1].replace(" UTC", ""), "%Y %b %d %H%M")
            pre = make_prediction(dt, kp)

            # Add yet unfilled values to prediction object
            pre["occurred"] = None
            pre["eventid"] = None

            # Check if it's already been processed
            # Get 10 most recent predictions
            last_pres = collection.find().sort("prediction_time", -1).limit(10)

            # Loop through predictions and check if any have same prediction_time as the new one
            go_ahead = True
            for i in last_pres:
                if i["prediction_time"] == pre["prediction_time"] and (len(sys.argv) == 1 and sys.argv[1] != "--test"):
                    go_ahead = False
                    break

            # Assuming all is well, add the new prediction to the db
            if go_ahead:
                collection.insert_one(pre)

    # This is where we check against previous prediction
    # When testing, ask the user weather they want to validate past predictions
    if (len(sys.argv) == 1 or sys.argv[1] != "--test") or raw_input("Validate: ") == "y":

        # Get 10 most recent predictions
        last_pres = collection.find().sort("prediction_time", -1).limit(10)

        # Loop through predictions
        today = str(datetime.utcnow())
        for i in last_pres:
            # Check that it hasnt been checked and that we wont need to time travel to check it
            if i["occurred"] == None and sorted([today, i["dates"][1]]).index(today) == 1:

                # If all is well, make a web request to the USGS to get all earthquakes in predicted timespan
                # Notice that we specify a time for the end date. This is because the dates default to being 0:00 in time
                eqtext = urllib2.urlopen('https://earthquake.usgs.gov/fdsnws/event/1/query?format=text&starttime=' + i["dates"][0] + '&endtime=' + i["dates"][1] + 'T23:59:59&minmagnitude=4').read()

                # Then loop over the events and check if they meet the criteria
                # It is possible that multiple events occur in the predicted timespan the meet the criteria. If this is the case, the one with the highest magnitude will be chosen.
                # EventID|Time|Latitude|Longitude|Depth/km|Author|Catalog|Contributor|ContributorID|MagType|Magnitude|MagAuthor|EventLocationName
                iscorrect = False
                lastmag = 0
                for event in eqtext.strip("\n").split("\n")[1:]:
                    edata = event.split("|")
                    # Check magnitude
                    if float(edata[10]) >= i["mags"][0] and float(edata[10]) >= lastmag:
                        # Check longitude
                        if check_lon_between(i["lons"][0], i["lons"][1], int(float(edata[3]))):

                            # Assign variables to confirm
                            iscorrect = True
                            lastmag = float(edata[10])
                            confirmed_event = edata[0]

                # If the event is confirmed to have occurred, change the document to reflect this
                # Otherwise, change it to show that it didnt occur
                if iscorrect:
                    collection.update_one({'prediction_time': str(i["prediction_time"])}, {"$set": {"occurred": True, "eventid": confirmed_event}})
                else:
                    collection.update_one({'prediction_time': str(i["prediction_time"])}, {"$set": {"occurred": False}})

    # If in test or offset mode, break after one iteration
    if len(sys.argv) == 2:
        break

    # Else, wait 5 minutes
    tting.sleep(300)
