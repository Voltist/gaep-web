from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_cors import CORS
import time
import urllib
from pymongo import MongoClient
import requests
import datetime
import random
import sys, os

# Setup flask server
app = Flask(__name__)
CORS(app)
api = Api(app)

# Connect to db
authstr = 'mongodb://%s:%s@localhost:27017/gaep' % ("admin", os.environ['gaep_db_pass'])
client = MongoClient(authstr)
db = client.gaep
collection = db.predictions

class main(Resource):
    def get(self):
        data = collection.find().sort("prediction_time", -1).limit(100)
        tmp = []
        for i in data:
            x = i
            del x["_id"]
            tmp.append(x)
        return tmp

api.add_resource(main, '/api')
if __name__ == '__main__':
     app.run(port='6660')
