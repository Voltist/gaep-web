const OCCURRED_TEXT = "OCCURED - MORE INFO";
const FAILED_TEXT = "DID NOT OCCUR";

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function to_usgs(id) {
  var url = "https://earthquake.usgs.gov/earthquakes/eventpage/{0}/executive".format($(id).data("id"));
  window.open(url, '_blank');
}

function display_past(past) {
  var html_string = '<p class="title">PAST PREDICTIONS</p>\n';
  for (var i in past) {
    var prediction = past[i];
    var mag_min = prediction.mags[0];
    var date = prediction.prediction_time.split(" ")[0];

    var button_html = '<button type="button" class="nes-btn is-error">DID NOT OCCUR</button>';
    if (prediction.occurred) {
      button_html = '<button type="button" class="nes-btn is-success" id="{0}" data-id="{1}" onclick="to_usgs(this);">OCCURED - MORE INFO</button>'
      .format(i, prediction.eventid);  // id will become the index in the past array, and data-id will be the USGS url id
    }
    html_string += `\n<div class="nes-container item">
      <div class="left">
        <h1>{0}+ on {1}</h1>
      </div>
      <div class="right">
        {2}
      </div>
    </div>\n`.format(mag_min, date, button_html);
  }
  $("#past_predictions").html(html_string);
}

function display_current(current) {
  var html_string = '<p class="title">ACTIVE PREDICTION</p>\n';
  var actual_current_id = -1;

  for (var i in current) {
    var prediction = current[i];
    var min_mag = prediction.mags[0];
    if (i > 0) {  //skip index 0
      if (min_mag > current[actual_current_id]) {
        actual_current_id = i;
      }
    } else {
      actual_current_id = 0;
    }
  }

  var mag_str = '<h1 id="no_current">All Clear!</h1>';
  var lng_str = '';
  var date_str = '';
  if (actual_current_id > -1) {
    var actual_current = current[actual_current_id];
    var mag = actual_current.mags[0];
    mag_str = '<h1 id="mag">{0}+</h1>'.format(mag);
    var min_lng = actual_current.lons[0];
    var max_lng = actual_current.lons[1];
    lng_str = '<h1>{0} to {1} degrees east</h1>'.format(min_lng, max_lng);
    var start_date = actual_current.dates[0];
    var stop_date = actual_current.dates[1];
    date_str = '<h1>{0} to {1} UTC'.format(start_date, stop_date);
  }
  html_string += `<div class="vertical-center">
    <div>
      {0}
      {1}
      {2}
    </div>
  </div>`.format(mag_str, lng_str, date_str);
  $("#current_prediction").html(html_string);
}

// on startup get predictions
$(document).ready(function() {
  $.get("https://gaep.liamsc.com/api", function(data) {  // newest first
    var present = [];
    var past = [];
    for (var i in data) {
      prediction = data[i]
      if (prediction.occurred == null) {  // if it is occurring
        present.push(prediction);
      } else {  // if it did or didn't happen
        past.push(prediction);
      }
    }
    display_current(present);
    display_past(past);
  });
});
